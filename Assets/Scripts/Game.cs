﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{

    public static World world;
    public static Tasker tasker;

    public TileList defaultTiles;
    public ItemList defaultItems;

    public Transform character;

    [HideInInspector]
    public static float WorldScreenWidth;
    [HideInInspector]
    public static float WorldScreenHeight;

    void Awake()
    {
        tasker = gameObject.GetComponent<Tasker>();
        world = GameObject.FindGameObjectWithTag("World").GetComponent<World>();

        TileTextureProvider.CreateTerrainAtlas(18 * 3);
    }

    void Start()
    {
        world.SetChunkOffset((int)Mathf.Round(character.position.x), (int)Mathf.Round(character.position.y));
        world.GenerateChunks();
    }

    public void OnChunkGenerated(int x, int y)
    {

    }

    void FixedUpdate()
    {
        world.SetChunkOffset((int)Mathf.Round(character.position.x), (int)Mathf.Round(character.position.y));
    }

    void Update()
    {
        WorldScreenHeight = Camera.main.orthographicSize * 2;
        WorldScreenWidth = (float)Screen.width / Screen.height * Camera.main.orthographicSize * 2;

        if (Input.GetKey(KeyCode.U))
        {
            world.MarkAllChunksDirty();
        }

        if (Input.GetKey(KeyCode.L))
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Debug.Log(Chunk.TileLightToValue(world.GetTileLight((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y))));
        }

        if (Input.GetKey(KeyCode.R))
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            world.SetTileLight((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y), 1.0f);

        }
        else if (Input.GetKey(KeyCode.T))
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            world.SetTileLight((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y), 0);
        }

        if (Input.GetMouseButton(1))
        {
            //System.IO.File.WriteAllBytes("C:/Users/Max/Desktop/TilesExp.png", TileTextureProvider.lightingAtlasTexture.EncodeToPNG());
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y), 2);

        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 clickPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x + 1), (int)Mathf.Round(clickPos.y), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x - 1), (int)Mathf.Round(clickPos.y), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y + 1), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y - 1), 0);

            world.SetTileById((int)Mathf.Round(clickPos.x + 1), (int)Mathf.Round(clickPos.y + 1), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x - 1), (int)Mathf.Round(clickPos.y + 1), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x + 1), (int)Mathf.Round(clickPos.y - 1), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x - 1), (int)Mathf.Round(clickPos.y - 1), 0);

            world.SetTileById((int)Mathf.Round(clickPos.x + 2), (int)Mathf.Round(clickPos.y), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x - 2), (int)Mathf.Round(clickPos.y), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y + 2), 0);
            world.SetTileById((int)Mathf.Round(clickPos.x), (int)Mathf.Round(clickPos.y - 2), 0);

        }
    }

}
