﻿using UnityEngine;
using System.Collections;

public class EntityLiving : MonoBehaviour {

    public float maxSpeed = 15f;
    public float jumpPower = 25f;
    public float jumpCooldown = 0.5f;
    public float maxFallSpeed = 75f;

    public Transform[] flippable;

    public Transform groundedPoint;
    public LayerMask groundLayers;
    public Transform autoStepPoint;
    public Transform autoStepEmpyPoint;

    private new Rigidbody2D rigidbody;
    private Animator animator;
    private float lastXDir = 0;
    private bool grounded;
    private bool jumping;
    protected Vector2Float targetDirection = new Vector2Float(0, 0);


    public void Start () {
        rigidbody = GetComponentInParent<Rigidbody2D>();
        animator = GetComponentInParent<Animator>();
    }
	
	public void FixedUpdate () {
        grounded = Physics2D.OverlapCircle(groundedPoint.position, 0.5f, groundLayers);

        if (targetDirection.x != 0 && Mathf.Sign(lastXDir) != Mathf.Sign(targetDirection.x))
        {
            lastXDir = targetDirection.x;
            FlipCharacter(targetDirection.x);
        }

        bool autoStep = targetDirection.x != 0 && grounded && targetDirection.x != 0 && Physics2D.OverlapCircle(autoStepPoint.position, 0.2f, groundLayers) && !Physics2D.OverlapCircle(autoStepEmpyPoint.position, 0.2f, groundLayers);

        rigidbody.velocity = new Vector2 (targetDirection.x * maxSpeed, (rigidbody.velocity.y < 0 ? Mathf.Max(rigidbody.velocity.y, -maxFallSpeed) : rigidbody.velocity.y) + (autoStep ? 2 : 0));
        if (autoStep)
        {
            rigidbody.transform.Translate(0, 0.25f, 0);
        }

        if (animator)
        {
            animator.SetFloat("Speed", Mathf.Abs(targetDirection.x));
            animator.SetFloat("vSpeed", Mathf.Abs(rigidbody.velocity.y));
            animator.SetFloat("vVelocity", rigidbody.velocity.y);
            animator.SetBool("Grounded", grounded);
        }
	}

    public bool IsGrounded ()
    {
        return grounded;
    }

    public void Jump ()
    {
        if (!jumping && IsGrounded ())
        {
            grounded = false;
            jumping = true;
            StartCoroutine(waitJumpDelay());
            rigidbody.AddForce(new Vector2(0, jumpPower));
        }    
    }

    IEnumerator waitJumpDelay ()
    {
        yield return new WaitForSeconds(jumpCooldown);
        jumping = false;
    }

    public void FlipCharacter (float dir)
    {
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * Mathf.Sign(dir), transform.localScale.y, transform.localScale.z);

        if (flippable == null)
            return;

        foreach (Transform t in flippable)
        {
            t.localPosition = new Vector3(t.localPosition.x, t.localPosition.y, -t.localPosition.z);
        }
    }
}

[System.Serializable]
public struct Vector2Float
{
    public float x;
    public float y;

    public Vector2Float(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public void set(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
}