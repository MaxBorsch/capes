﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public static class Texture2DExtensions
{
    public enum Rotation { Left, Right, HalfCircle }
    public static void Rotate(this Texture2D texture, Rotation rotation)
    {
        Color32[] originalPixels = texture.GetPixels32();
        IEnumerable<Color32> rotatedPixels;

        if (rotation == Rotation.HalfCircle)
            rotatedPixels = originalPixels.Reverse();
        else
        {
            // Rotate left:
            var firstRowPixelIndeces = Enumerable.Range(0, texture.height).Select(i => i * texture.width).Reverse().ToArray();
            rotatedPixels = Enumerable.Repeat(firstRowPixelIndeces, texture.width).SelectMany(
                (frpi, rowIndex) => frpi.Select(i => originalPixels[i + rowIndex])
            );

            if (rotation == Rotation.Right)
                rotatedPixels = rotatedPixels.Reverse();
        }

        texture.SetPixels32(rotatedPixels.ToArray());
        texture.Apply();
    }

    public static Color32[] rotateSquare(Color32[] origPixels, int size, int rot)
    {
        if (rot == 0 || rot == 360 || rot == -360)
            return origPixels;

        int x;
        int y;
        Color32[] rotPixels = new Color32[size * size];

        for (x = 0; x < size; x++)
        {
            for (y = 0; y < size; y++)
            {
                if (rot == 270 || rot == -90)
                    rotPixels[XY(x, y, size)] = origPixels[XY(y, (size - 1 - x), size)];

                if (rot == 90)
                    rotPixels[XY(x, y, size)] = origPixels[XY((size - 1 - y), x, size)];

                if (rot == 180)
                    rotPixels[XY(x, y, size)] = origPixels[XY(size - 1 - x, size - 1 - y, size)];

            }
        }

        return rotPixels;
    }

    public static Color32[] rotateSquare(Texture2D texture, int rot)
    {
        return rotateSquare(texture.GetPixels32(), texture.width, rot);
    }

    public static Color32[] ConvertToGrayscale (Texture2D texture)
    {
        Color32[] pixels = texture.GetPixels32();
        for (int x = 0; x < texture.width; x++)
        {
            for (int y = 0; y < texture.height; y++)
            {
                Color32 pixel = pixels[x + y * texture.width];
                int p = ((256 * 256 + pixel.r) * 256 + pixel.b) * 256 + pixel.g;
                int b = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int g = p % 256;
                p = Mathf.FloorToInt(p / 256);
                int r = p % 256;
                float l = (0.2126f * r / 255f) + 0.7152f * (g / 255f) + 0.0722f * (b / 255f);
                Color c = new Color(l, l, l, 1);
                texture.SetPixel(x, y, c);
            }
        }
        texture.Apply(false);

        return pixels;
    }

    public static Color32[] FillColor(Color32[] c, int w, int h, Color co)
    {
        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                c[y * w + x] = co;
            }
        }

        return c;
    }

    public static int XY( int x, int y, int width)
    {
        return (x + y * width);
    }
}