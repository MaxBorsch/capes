﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public Transform target;
    public Light sun;
    public int sunDistance = 0;
    public int sunHeight = 50;
    public int viewportScale = 32;

    private new Camera camera;

	void Start () {
		camera = gameObject.GetComponent <Camera> ();
	}
	
	void Update () {
		camera.orthographicSize = Screen.height / viewportScale;
        camera.transform.position = target.position + new Vector3(0, 0, -20);

        if (sun.gameObject.activeSelf && camera.transform.position.y < sunDistance)
            sun.gameObject.SetActive(false);
        else if (!sun.gameObject.activeSelf && camera.transform.position.y > sunDistance)
            sun.gameObject.SetActive(true);

        if (sun.gameObject.activeSelf)
            sun.transform.localPosition = new Vector3(camera.transform.localPosition.x, sunHeight, -20);

    }
}
