using System;
using System.Collections.Generic;

public class DecoupledPriorityQueue<T>
{
    private struct PriorityQueueItem : IComparable
    {
        public readonly T item;
        private readonly int priority;

        public PriorityQueueItem(T item, int priority)
        {
            this.item = item;
            this.priority = priority;
        }

        public int CompareTo(object other)
        {
            return priority.CompareTo(((PriorityQueueItem) other).priority);
        }
    }

    private readonly PriorityQueue<PriorityQueueItem> innerQueue = new PriorityQueue<PriorityQueueItem>();
    private readonly HashSet<T> containedItems = new HashSet<T>();
    public int Count;

    public void Enqueue(T item, int priority)
    {
        innerQueue.Enqueue(new PriorityQueueItem(item, priority));
        containedItems.Add(item);
        Count++;
    }

    public T Dequeue()
    {
        Count--;
        T item = innerQueue.Dequeue().item;
        containedItems.Remove(item);
        return item;
    }

    public bool Contains(T c)
    {
        return containedItems.Contains(c);
    }
}