﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sky : MonoBehaviour
{
    public Transform satellite;
    public Sprite sun;
    public Sprite moon;
    public Material sky;
    public Color atmosphereDay;
    public Color atmosphereEvening;
    public Color atmosphereNight;
    public float timeScale = 1;
    public float timeOfDay = 0;

    private SpriteRenderer satelliteRenderer;
    private ParticleSystem satelliteParticleSystem;

    void Start()
    {
        satelliteRenderer = satellite.GetComponent<SpriteRenderer>();
        satelliteParticleSystem = satellite.GetComponent<ParticleSystem>();
    }

    void Update()
    {
        timeOfDay += Time.fixedDeltaTime * timeScale / 60 / 60 / 24;

        if (timeOfDay > 1)
        {
            timeOfDay = 0;
        }
        else if (timeOfDay < 0)
        {
            timeOfDay = 0;
        }

        if ((timeOfDay > 0.1 && timeOfDay < 0.3) || (timeOfDay > 0.6 && timeOfDay < 0.7))
        {
            if (!satelliteParticleSystem.isPlaying)
            {
                satelliteParticleSystem.Play();
            }
        }
        else if (satelliteParticleSystem.isPlaying)
        {
            satelliteParticleSystem.Stop();
        }

        if (timeOfDay < 0.25)
        {
            sky.color = Color.Lerp(atmosphereEvening, atmosphereDay, Mathf.Repeat(timeOfDay, 0.25f) / 0.25f);
        }
        else if (timeOfDay < 0.5)
        {
            sky.color = Color.Lerp(atmosphereDay, atmosphereEvening, Mathf.Repeat(timeOfDay, 0.25f) / 0.25f);
        }
        else if (timeOfDay < 0.75)
        {
            sky.color = Color.Lerp(atmosphereEvening, atmosphereNight, Mathf.Repeat(timeOfDay, 0.25f) / 0.25f);
        }
        else
        {
            sky.color = Color.Lerp(atmosphereNight, atmosphereEvening, Mathf.Repeat(timeOfDay, 0.25f) / 0.25f);
        }

        satelliteRenderer.sprite = timeOfDay < 0.5f ? sun : moon;

        satellite.localPosition = new Vector3((Game.WorldScreenWidth / 2 - 2) * Mathf.Cos(Mathf.Repeat(timeOfDay, 0.5f) * Mathf.PI * 2), (Game.WorldScreenHeight / 2 - 2) * Mathf.Sin(Mathf.Repeat(timeOfDay, 0.5f) * Mathf.PI * 2), 0);
        satelliteRenderer.material.color = new Color(1, 1, 1, Mathf.Sin(Mathf.Repeat(timeOfDay, 0.5f) * Mathf.PI * 2.2f));
    }
}
