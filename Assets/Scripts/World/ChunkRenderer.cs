﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class ChunkRenderer : MonoBehaviour
{
    public int TileWidth = 16;
    public int TileHeight = 16;
    public int NumTilesX = 16;
    public int NumTilesY = 16;
    public int TileGridWidth = 100;
    public int TileGridHeight = 100;
    public int DefaultTileX;
    public int DefaultTileY;
    public Texture2D Texture;

    private MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    private MaterialPropertyBlock materialPropertyBlock;
    private float[] lights;
    private bool loaded = false;
    public bool configured = false;

    void Awake()
    {
        Load();
    }

    public void Load()
    {
        if (!configured || !gameObject.activeSelf || loaded)
        {
            return;
        }

        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        materialPropertyBlock = new MaterialPropertyBlock();
        lights = new float[16 * 16];
        materialPropertyBlock.SetFloatArray("_Lights", lights);
        meshRenderer.SetPropertyBlock(materialPropertyBlock);

        if (Texture)
            SetTexture(Texture);

        CreatePlane(TileWidth, TileHeight, TileGridWidth, TileGridHeight);
        loaded = true;
    }

    public void SetTileLight(int x, int y, float light)
    {
        if (!loaded) return;

        lights[y * 16 + x] = light;
    }

    public void ApplyLighting()
    {
        materialPropertyBlock.SetFloatArray("_Lights", lights);
        meshRenderer.SetPropertyBlock(materialPropertyBlock);
    }

    public void UpdateTile(int x, int y, int tile)
    {
        if (!loaded) return;

        UpdateGrid(new Vector2(x, y), new Vector2(tile, 1), TileWidth, TileHeight, TileGridWidth, 1);
    }

    public void UpdateGrid(Vector2 gridIndex, Vector2 tileIndex, int tileWidth, int tileHeight, int gridWidth, int uvSet)
    {
        var mesh = meshFilter.mesh;
        var uvs = uvSet == 1 ? mesh.uv : uvSet == 2 ? mesh.uv2 : uvSet == 3 ? mesh.uv3 : uvSet == 4 ? mesh.uv4 : mesh.uv;

        var tileSizeX = 1.0f / NumTilesX;
        var tileSizeY = 1.0f / NumTilesY;

        uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 0] = new Vector2(tileIndex.x * tileSizeX, tileIndex.y * tileSizeY);
        uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 1] = new Vector2((tileIndex.x + 1) * tileSizeX, tileIndex.y * tileSizeY);
        uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 2] = new Vector2((tileIndex.x + 1) * tileSizeX, (tileIndex.y + 1) * tileSizeY);
        uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 3] = new Vector2(tileIndex.x * tileSizeX, (tileIndex.y + 1) * tileSizeY);

        switch (uvSet)
        {
            case 1:
                mesh.uv = uvs;
                break;
            case 2:
                mesh.uv2 = uvs;
                break;
            case 3:
                mesh.uv3 = uvs;
                break;
            case 4:
                mesh.uv4 = uvs;
                break;
            default:
                mesh.uv = uvs;
                break;
        }
    }

    public void UpdateTiles(Vector2[] gridIndexes, Vector2[] tileIndexes, int uvSet)
    {
        if (!loaded) return;

        UpdateGrid(gridIndexes, tileIndexes, TileWidth, TileHeight, TileGridWidth, uvSet);
    }

    public void UpdateGrid(Vector2[] gridIndexes, Vector2[] tileIndexes, int tileWidth, int tileHeight, int gridWidth, int uvSet)
    {
        var mesh = meshFilter.mesh;
        var uvs = uvSet == 1 ? mesh.uv : uvSet == 2 ? mesh.uv2 : uvSet == 3 ? mesh.uv3 : uvSet == 4 ? mesh.uv4 : mesh.uv;

        var tileSizeX = 1.0f / NumTilesX;
        var tileSizeY = 1.0f / NumTilesY;

        int updates = gridIndexes.Length;
        int i;
        Vector2 gridIndex;
        Vector2 tileIndex;

        for (i = 0; i < updates; i++)
        {
            gridIndex = gridIndexes[i];
            tileIndex = tileIndexes[i];

            uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 0] = new Vector2(tileIndex.x * tileSizeX, tileIndex.y * tileSizeY);
            uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 1] = new Vector2((tileIndex.x + 1) * tileSizeX, tileIndex.y * tileSizeY);
            uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 2] = new Vector2((tileIndex.x + 1) * tileSizeX, (tileIndex.y + 1) * tileSizeY);
            uvs[(int)(gridWidth * gridIndex.x + gridIndex.y) * 4 + 3] = new Vector2(tileIndex.x * tileSizeX, (tileIndex.y + 1) * tileSizeY);
        }

        switch (uvSet)
        {
            case 1:
                mesh.uv = uvs;
                break;
            case 2:
                mesh.uv2 = uvs;
                break;
            case 3:
                mesh.uv3 = uvs;
                break;
            case 4:
                mesh.uv4 = uvs;
                break;
            default:
                mesh.uv = uvs;
                break;
        }
    }

    public void UpdateTiles(int[] tileIndexes, int uvSet)
    {
        if (!loaded) return;

        UpdateGrid(tileIndexes, uvSet);
    }

    public void UpdateGrid(int[] tiles, int uvSet)
    {
        var mesh = meshFilter.mesh;
        var uvs = uvSet == 1 ? mesh.uv : uvSet == 2 ? mesh.uv2 : uvSet == 3 ? mesh.uv3 : uvSet == 4 ? mesh.uv4 : mesh.uv;

        var tileSizeX = 1.0f / NumTilesX;
        var tileSizeY = 1.0f / NumTilesY;

        var updates = tiles.Length;
        int i;
        int tile;

        for (i = 0; i < updates; i++)
        {
            tile = tiles[i];

            uvs[i * 4 + 0] = new Vector2(tile * tileSizeX, tileSizeY);
            uvs[i * 4 + 1] = new Vector2((tile + 1) * tileSizeX, tileSizeY);
            uvs[i * 4 + 2] = new Vector2((tile + 1) * tileSizeX, 2 * tileSizeY);
            uvs[i * 4 + 3] = new Vector2(tile * tileSizeX, 2 * tileSizeY);
        }

        switch (uvSet)
        {
            case 1:
                mesh.uv = uvs;
                break;
            case 2:
                mesh.uv2 = uvs;
                break;
            case 3:
                mesh.uv3 = uvs;
                break;
            case 4:
                mesh.uv4 = uvs;
                break;
            default:
                mesh.uv = uvs;
                break;
        }
    }

    public void SetTexture(Texture2D tex)
    {
        Texture = tex;
        if (meshRenderer)
            meshRenderer.material.SetTexture("_MainTex", Texture);
    }

    public bool isLoaded()
    {
        return loaded;
    }

    void CreatePlane(int tileHeight, int tileWidth, int gridHeight, int gridWidth)
    {
        var mesh = new Mesh();
        meshRenderer.material.SetTexture("_MainTex", Texture);
        meshFilter.mesh = mesh;

        var tileSizeX = 1.0f / NumTilesX;
        var tileSizeY = 1.0f / NumTilesY;

        var vertices = new List<Vector3>();
        var triangles = new List<int>();
        var normals = new List<Vector3>();
        var uvs = new List<Vector2>();

        var index = 0;
        for (var x = 0; x < gridWidth; x++)
        {
            for (var y = 0; y < gridHeight; y++)
            {
                AddVertices(tileHeight, tileWidth, y, x, vertices);
                index = AddTriangles(index, triangles);
                AddNormals(normals);
                AddUvs(DefaultTileX, tileSizeY, tileSizeX, uvs, DefaultTileY);
            }
        }

        mesh.vertices = vertices.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
    }

    private static void AddVertices(int tileHeight, int tileWidth, int y, int x, ICollection<Vector3> vertices)
    {
        vertices.Add(new Vector3((x * tileWidth), (y * tileHeight), 0));
        vertices.Add(new Vector3((x * tileWidth) + tileWidth, (y * tileHeight), 0));
        vertices.Add(new Vector3((x * tileWidth) + tileWidth, (y * tileHeight) + tileHeight, 0));
        vertices.Add(new Vector3((x * tileWidth), (y * tileHeight) + tileHeight, 0));
    }

    private static int AddTriangles(int index, ICollection<int> triangles)
    {
        triangles.Add(index + 2);
        triangles.Add(index + 1);
        triangles.Add(index);
        triangles.Add(index);
        triangles.Add(index + 3);
        triangles.Add(index + 2);
        index += 4;
        return index;
    }

    private static void AddNormals(ICollection<Vector3> normals)
    {
        normals.Add(Vector3.forward);
        normals.Add(Vector3.forward);
        normals.Add(Vector3.forward);
        normals.Add(Vector3.forward);
    }

    private static void AddUvs(int tileRow, float tileSizeY, float tileSizeX, ICollection<Vector2> uvs, int tileColumn)
    {
        uvs.Add(new Vector2(tileColumn * tileSizeX, tileRow * tileSizeY));
        uvs.Add(new Vector2((tileColumn + 1) * tileSizeX, tileRow * tileSizeY));
        uvs.Add(new Vector2((tileColumn + 1) * tileSizeX, (tileRow + 1) * tileSizeY));
        uvs.Add(new Vector2(tileColumn * tileSizeX, (tileRow + 1) * tileSizeY));
    }
}