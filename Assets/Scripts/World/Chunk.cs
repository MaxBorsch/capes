﻿using System.Collections.Generic;
using UnityEngine;

public class Chunk
{

    public World world;
    private readonly int[,] tiles;
    private readonly byte[,] lights;
    public Vector2Int position;
    private ChunkRenderer chunkRenderer;
    private BoxCollider2D[] colliders;
    public readonly Dictionary<int, int> tileBuckets;

    public GameObject gameObject;
    public int ChunkSize;
    public int TileSize;
    public bool cached;
    public bool displayed;
    public bool textured;
    public bool collidable;
    public bool instantiated;

    public Chunk(int x, int y)
    {
        world = Game.world;
        ChunkSize = world.ChunkSize;
        TileSize = world.TileSize;
        tiles = new int[world.ChunkSize, world.ChunkSize];
        lights = new byte[world.ChunkSize, world.ChunkSize];
        position = new Vector2Int(x, y);
        tileBuckets = new Dictionary<int, int>();
        tileBuckets.Add(0, ChunkSize * ChunkSize);
    }

    public void Instantiate()
    {
        if (gameObject == null)
        {
            gameObject = Object.Instantiate(world.emptyChunk);
            gameObject.transform.SetParent(GameObject.FindGameObjectWithTag("World").transform);
            gameObject.transform.localScale = new Vector3(1, 1, 1);
        }

        gameObject.name = "Chunk [" + position.x + ", " + position.y + "]";
        gameObject.transform.localPosition = new Vector3(position.x * world.ChunkSize - 0.5f, position.y * world.ChunkSize - 0.5f);
        gameObject.SetActive(true);

        if (chunkRenderer == null)
        {
            chunkRenderer = gameObject.GetComponent<ChunkRenderer>();
            chunkRenderer.NumTilesX = TileTextureProvider.atlasTexture.width / TileSize;
            chunkRenderer.NumTilesY = 1;
            chunkRenderer.TileGridWidth = chunkRenderer.TileGridHeight = Game.world.ChunkSize;
            chunkRenderer.SetTexture(TileTextureProvider.atlasTexture);
            chunkRenderer.configured = true;
            chunkRenderer.Load();
        }

        instantiated = true;
    }

    public static int TileLightToValue(float light)
    {
        return (int)(light * 15f);
    }

    public static float TileLightValueToLight(int lightValue)
    {
        return lightValue / 15f;
    }

    public float GetTileSunlight(int x, int y)
    {
        return TileLightValueToLight(GetTileSunlightValue(x, y));
    }

    // Get Sunlight bits XXXX0000
    public int GetTileSunlightValue(int x, int y)
    {
        return lights[x, y] >> 4;
    }

    public void SetTileSunlight(int x, int y, float light)
    {
        SetTileSunlightValue(x, y, TileLightToValue(light));
    }

    // Set Sunlight bits XXXX0000
    public void SetTileSunlightValue(int x, int y, int val)
    {
        lights[x, y] = (byte)((lights[x, y] & 0xF) | (val << 4));
    }

    public float GetTileLight(int x, int y)
    {
        return TileLightValueToLight(GetTileLightValue(x, y));
    }

    // Get Light bits 0000XXXX
    public int GetTileLightValue(int x, int y)
    {
        return lights[x, y] & 0x0F;
    }

    public void SetTileLight(int x, int y, float light)
    {
        SetTileLightValue(x, y, TileLightToValue(light));
    }

    // Set Light bits 0000XXXX
    public void SetTileLightValue(int x, int y, int light)
    {
        lights[x, y] = (byte)((lights[x, y] & 0xF0) | light);

        chunkRenderer.SetTileLight(x, y, TileLightValueToLight(light));
    }

    public Tile GetTile(int x, int y)
    {
        return world.GetTileFromId(tiles[x, y]);
    }

    public void UpdateTile(int x, int y, Tile lastTile, bool otherChunk = false)
    {
        if (textured)
        {
            if (x < world.ChunkSize && x >= 0 && y < world.ChunkSize && y >= 0)
            {
                UpdateTileTexture(x, y);
            }

            if (!otherChunk && x >= 0 && x < world.ChunkSize && y >= 0 && y < world.ChunkSize)
            {
                if (x + 1 < world.ChunkSize)
                {
                    UpdateTileTexture((x + 1), y);
                    UpdateTileCollider((x + 1), y, tiles[(x + 1), y]);
                }

                if (x - 1 >= 0)
                {
                    UpdateTileTexture((x - 1), y);
                    UpdateTileCollider((x - 1), y, tiles[(x - 1), y]);
                }

                if (y + 1 < world.ChunkSize)
                {
                    UpdateTileTexture(x, (y + 1));
                    UpdateTileCollider(x, (y + 1), tiles[x, (y + 1)]);
                }

                if (y - 1 >= 0)
                {
                    UpdateTileTexture(x, (y - 1));
                    UpdateTileCollider(x, (y - 1), tiles[x, (y - 1)]);
                }
            }

            UpdateTileCollider(x, y, tiles[x, y]);
        }
    }

    public void SetTile(int x, int y, Tile tile)
    {
        int currentTile = tiles[x, y];
        tiles[x, y] = tile.id;
        tileBuckets[currentTile]--;
        if (tileBuckets.ContainsKey(tile.id))
        {
            tileBuckets[tile.id]++;
        }
        else
        {
            tileBuckets.Add(tile.id, 1);
        }

    }

    public void ToggleColliders(bool on)
    {
        if (on && !collidable)
        {
            collidable = true;

            if (colliders != null)
            {
                foreach (BoxCollider2D collider in colliders)
                {
                    collider.enabled = true;
                }

                return;
            }

            colliders = new BoxCollider2D[world.ChunkSize * world.ChunkSize];
            for (int x = 0; x < world.ChunkSize; x++)
            {
                for (int y = 0; y < world.ChunkSize; y++)
                {
                    if (tiles[x, y] > 0 && world.IsBorderTile(position.x * world.ChunkSize + x, position.y * world.ChunkSize + y))
                    {
                        UpdateTileCollider(x, y, tiles[x, y]);
                    }
                }
            }
        }

        else if (!on && collidable)
        {
            collidable = false;
            foreach (BoxCollider2D collider in colliders)
            {
                collider.enabled = false;
            }
        }

    }

    void UpdateTileCollider(int x, int y, int tile)
    {
        if (!collidable)
            return;

        if (tile == 0 && colliders[world.ChunkSize * y + x] != null) // Remove Collider
        {
            colliders[world.ChunkSize * y + x].enabled = false;
        }
        else if (tile != 0 && colliders[world.ChunkSize * y + x] != null) // Update collider
        {
            colliders[world.ChunkSize * y + x].enabled = true;
        }
        else if (tile != 0 && colliders[world.ChunkSize * y + x] == null) // Add Collider
        {
            BoxCollider2D collider = gameObject.AddComponent<BoxCollider2D>();
            collider.size = new Vector2(1, 1);
            collider.offset = new Vector2(x + 0.6f, y + 0.6f);
            colliders[world.ChunkSize * y + x] = collider;
        }
    }

    public void Display(bool active)
    {
        if (active)
        {
            chunkRenderer.meshRenderer.enabled = true;
            displayed = true;
        }
        else if (!active)
        {
            chunkRenderer.meshRenderer.enabled = false;
            displayed = false;
        }
    }

    public void Unload()
    {
        if (gameObject)
            Object.Destroy(gameObject);
    }

    public void Cache()
    {
        cached = true;

        if (displayed)
        {
            Display(false);
        }

        for (int x = 0; x < ChunkSize; x++)
        {
            for (int y = 0; y < ChunkSize; y++)
            {
                tiles[x, y] = 0;
                lights[x, y] = 0;
                if (chunkRenderer != null)
                {
                    chunkRenderer.SetTileLight(x, y, 0);
                }
            }
        }

        tileBuckets.Clear();
        tileBuckets.Add(0, ChunkSize * ChunkSize);

        if (instantiated)
        {
            gameObject.SetActive(false);
            gameObject.name = "Chunk [Cached]";

            if (colliders != null)
            {
                foreach (BoxCollider2D collider in colliders)
                {
                    Object.Destroy(collider);
                }
                colliders = null;
            }

            collidable = false;
        }

        textured = false;
    }

    float GetLight(int x, int y)
    {
        return lights[x, y] / 16f;
    }

    public void DrawTiles()
    {
        int[] tileTexIds = new int[world.ChunkSize * world.ChunkSize];

        for (int x = 0; x < world.ChunkSize; x++)
        {
            for (int y = 0; y < world.ChunkSize; y++)
            {
                tileTexIds[x * world.ChunkSize + y] = (tiles[x, y] == 0 ? 0 : world.GetTileFromId(tiles[x, y]).texture.Pick(x, y, world.GetTileBorders(position.x * world.ChunkSize + x, position.y * world.ChunkSize + y)));
            }
        }

        chunkRenderer.UpdateTiles(tileTexIds, 1);
        chunkRenderer.ApplyLighting();
    }

    public void ApplyLighting()
    {
        chunkRenderer.ApplyLighting();
    }

    public void UpdateTileTexture(int x, int y)
    {
        chunkRenderer.UpdateTile(x, y, (tiles[x, y] == 0 ? 0 : world.GetTileFromId(tiles[x, y]).texture.Pick(x, y, world.GetTileBorders(position.x * world.ChunkSize + x, position.y * world.ChunkSize + y))));
    }

    public bool needsTexture()
    {
        return !textured && chunkRenderer.isLoaded();
    }

    public void UpdateTexture()
    {
        textured = true;

        DrawTiles();
    }

    public override int GetHashCode()
    {
        return position.GetHashCode();
    }
}
