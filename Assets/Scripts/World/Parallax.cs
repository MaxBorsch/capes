﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float speed = 1f;
    public bool loopX;
    public bool loopY;
    public Transform target;

    private Vector3 lastTargetPosition;

    void Start()
    {
        lastTargetPosition = target.position;
    }

    void Update()
    {
        Vector3 targetDelta = target.position - lastTargetPosition;
        transform.Translate(targetDelta * speed);
        lastTargetPosition = target.position;
    }
}
