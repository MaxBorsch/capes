﻿using UnityEngine;
using System.Collections.Generic;

public class TileTexture
{
    public Texture2D original;
    public Texture2D rotated;
    public int originalIndex;
    public int rotatedIndex;
    public Dictionary<int, int> border;

    public TileTexture (Texture2D texture)
    {
        original = texture;
        originalIndex = TileTextureProvider.AddTexture(original);

        rotated = new Texture2D(texture.width, texture.height);
        rotated.SetPixels32(Texture2DExtensions.rotateSquare(texture, 270));
        rotated.Apply();
        rotatedIndex = TileTextureProvider.AddTexture(rotated);
        rotated.Compress(false);

        border = new Dictionary<int, int>();
    }

    int BoolArrayToInt(bool[] bits)
    {
        if (bits.Length > 32) throw new System.ArgumentException("Can only fit 32 bits in a int");

        int r = 0;
        for (int i = 0; i < bits.Length; i++) if (bits[i]) r |= 1 << (bits.Length - i);
        return r;
    }

    int GetBorderedTile(Texture2D texture, bool[] borders)
    {
        int borderKey = BoolArrayToInt(borders);

        if (border.ContainsKey (borderKey))
            return border[borderKey];


        Texture2D newTex = new Texture2D(texture.width, texture.height, TextureFormat.ARGB32, false);
        newTex.SetPixels32(GenerateBorders(texture.GetPixels32(), texture.width, borders));
        int atlasIndex = TileTextureProvider.AddTexture(newTex);
        border.Add(borderKey, atlasIndex);

        return atlasIndex;
    }

    Color32[] GenerateBorders(Color32[] pixels, int size, bool[] borders)
    {
        int borderCount = 0;
        int firstBorder = -1;
        int firstSmooth = -1;
        for (int b = 0; b < 4; b++)
        {
            if (borders[b])
            {
                if (firstBorder == -1)
                    firstBorder = b;

                borderCount++;
            }
            else if (firstSmooth == -1)
                firstSmooth = b;
        }

        bool opposites = (borderCount == 2 && ((borders[1] && borders[3]) || (borders[0] && borders[2])));

        DrawBorders(ref pixels, size, borderCount, opposites);

        if (borderCount == 4)
            return pixels;

        if (opposites)
        {
            if (borders[1])
                pixels = Texture2DExtensions.rotateSquare(pixels, size, 90);

            return pixels;
        }

        int rot = 0;

        if (borderCount == 1)
        {
            rot = firstBorder * 90;

        }else if (borders[0] && borders[1] && borderCount == 2)
        {
            rot = 0;

        } else if (borders[1] && borders[2] && borderCount == 2)
        {
            rot = 90;

        } else if (borders[2] && borders[3] && borderCount == 2)
        {
            rot = 180;
        }
        else if (borders[3] && borders[0] && borderCount == 2)
        {
            rot = -90;
        }
        else if (borderCount == 3)
        {
            rot = (firstSmooth+1) * 90;
        }


        pixels = Texture2DExtensions.rotateSquare(pixels, size, rot);

        return pixels;
    }

    void SetMegapixel (ref Color32[] pixels, int size, int x, int y, Color32 color)
    {
        pixels[Texture2DExtensions.XY(x, y, size)] = color;
        pixels[Texture2DExtensions.XY(x + 1, y, size)] = color;
        pixels[Texture2DExtensions.XY(x, y + 1, size)] = color;
        pixels[Texture2DExtensions.XY(x + 1, y + 1, size)] = color;
    }


    Color transparent = Color.clear;
    Color black = Color.black;
    void DrawBorders (ref Color32[] pixels, int size, int borderCount, bool opposites)
    {
        for (int b = 0; b < borderCount; b++)
        {
            if (b > 0)
                pixels = Texture2DExtensions.rotateSquare(pixels, size, opposites ? 180 : 90);

            for (int x = 0; x < size; x+=2)
            {
                if (x == 0 || x == size - 2 || Random.Range(0, 3) > 0)
                {
                    SetMegapixel(ref pixels, size, x, size - 2, black);

                } else
                {
                    SetMegapixel(ref pixels, size, x, size - 2, transparent);
                    SetMegapixel(ref pixels, size, x, size - 4, black);
                }
            }
        }
    }

    // borders: up, right, down, left
    public int Pick(int x, int y, bool[] borders)
    {
        Random.InitState (x * y);
        if (!(borders[0] || borders[1] || borders[2] || borders[3]))
            return Random.Range(0, 2) == 0 ? originalIndex : rotatedIndex;


        return GetBorderedTile (Random.Range(0, 2) == 0 ? original : rotated, borders);
    }
}