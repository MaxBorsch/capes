﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundDecorator : MonoBehaviour
{

    public Transform mountain;

    private Dictionary<Chunk, List<Transform>> decorations;

    public BackgroundDecorator()
    {
        decorations = new Dictionary<Chunk, List<Transform>>();
    }

    void OnEnable()
    {
        World.OnChunkLoaded += DecorateChunk;
        World.OnChunkUnloaded += UndecorateChunk;
    }


    void OnDisable()
    {
        World.OnChunkLoaded -= DecorateChunk;
        World.OnChunkUnloaded -= UndecorateChunk;
    }

    private void UndecorateChunk(Chunk chunk)
    {
        if (decorations.ContainsKey(chunk))
        {
            foreach (Transform t in decorations[chunk])
            {
                Destroy(t.gameObject);
            }
            decorations.Remove(chunk);
        }
    }

    private void DecorateChunk(Chunk chunk)
    {
        List<Transform> decorationList;
        if (!decorations.TryGetValue(chunk, out decorationList))
        {
            decorationList = new List<Transform>();
        }

        if (chunk.position.y == 0)
        {
            Transform m = Instantiate(mountain, gameObject.transform);
            m.localScale = new Vector3(Random.Range(20, 25), Random.Range(20, 25), 1);
            m.position = chunk.gameObject.transform.position + new Vector3(0, Random.Range(5, 12), 0);
            m.GetComponent<Parallax>().target = Camera.main.transform;
            m.GetComponent<Parallax>().speed = Random.Range(60, 90) / 100f;

            decorationList.Add(m);
        }

        decorations.Add(chunk, decorationList);
    }
}
