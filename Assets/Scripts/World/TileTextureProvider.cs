﻿using UnityEngine;
using System.Collections.Generic;

public static class TileTextureProvider
{

    // List of atlas indexes available for overwrite
    public static List<int> openTextures = new List<int>();

    // Atlas Texture
    public static Texture2D atlasTexture;


    public static void CreateTerrainAtlas(int atlasSize)
    {
        atlasTexture = new Texture2D(atlasSize * 16, 16, TextureFormat.ARGB32, false);
        atlasTexture.wrapMode = TextureWrapMode.Repeat;
        atlasTexture.filterMode = FilterMode.Point;

        atlasTexture.SetPixels32(0, 0, 16, 16, Texture2DExtensions.FillColor(new Color32[16 * 16], 16, 16, Color.clear));
        openTextures.Add(1);
    }

    public static int AddTexture(Texture2D texture)
    {
        if (openTextures.Count < 1) // First tile at 0
            openTextures.Add(0);

        int index = openTextures[0];

        atlasTexture.SetPixels32(index * Game.world.TileSize, 0, Game.world.TileSize, Game.world.TileSize, texture.GetPixels32());
        atlasTexture.Apply(false);

        openTextures.RemoveAt(0);
        if (openTextures.Count < 1) // If last index, open next one
            openTextures.Add(index + 1);

        return index;
    }
}
