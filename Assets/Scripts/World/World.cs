﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class World : MonoBehaviour
{
    public int ChunkSize = 32;
    public int TileSize = 16;
    public Vector2Int LoadDistance = new Vector2Int(6, 3);
    public Vector2Int RenderDistance = new Vector2Int(4, 3);
    public GameObject emptyChunk;

    private List<Tile> tiles;
    private Dictionary<string, Tile> tileNameMap;
    private Vector2Int chunkOffset;
    private Dictionary<Vector2Int, Chunk> chunks;
    private Queue<Chunk> cachedChunks;

    private DecoupledPriorityQueue<Vector2Int> dirtyChunks;
    private HashSet<Chunk> lightProcessedChunks = new HashSet<Chunk>();

    private PriorityQueue<LightNode> lightBfsQueue;
    private Queue<LightRemovalNode> lightRemovalBfsQueue;

    [HideInInspector]
    public Tile air;

    public delegate void ChunkLoadedEvent(Chunk chunk);
    public static event ChunkLoadedEvent OnChunkLoaded;

    public delegate void ChunkUnloadedEvent(Chunk chunk);
    public static event ChunkUnloadedEvent OnChunkUnloaded;

    public World()
    {
        tiles = new List<Tile>();
        tileNameMap = new Dictionary<string, Tile>();
        chunks = new Dictionary<Vector2Int, Chunk>();
        cachedChunks = new Queue<Chunk>();
        dirtyChunks = new DecoupledPriorityQueue<Vector2Int>();
        chunkOffset = new Vector2Int(0, 0);
        lightBfsQueue = new PriorityQueue<LightNode>();
        lightRemovalBfsQueue = new Queue<LightRemovalNode>();

        air = new Tile();
        air.name = "air";
        air.id = 0;
        tiles.Add(air);
        tileNameMap.Add(air.name, air);
    }

    public void Start()
    {
        GenerateChunks();
        StartCoroutine(UpdateChunks());

        InvokeRepeating("CleanChunks", 10, 10);
    }

    private IEnumerator UpdateChunks()
    {
        while (dirtyChunks.Count > 0)
        {
            CleanChunk(dirtyChunks.Dequeue());

            yield return new WaitForSeconds(0.1f);
        }
    }

    public bool SetChunkOffset(int x, int y)
    {
        if (x < 0)
            x -= ChunkSize;
        if (y < 0)
            y -= ChunkSize;

        int oldX = chunkOffset.x;
        int oldY = chunkOffset.y;
        chunkOffset.set(x / ChunkSize, y / ChunkSize);

        bool needUpdate = (oldX != chunkOffset.x || oldY != chunkOffset.y);

        if (needUpdate)
        {
            GenerateChunks();
            UpdateChunkOffset(chunkOffset.x - oldX, chunkOffset.y - oldY);

            StartCoroutine(UpdateChunks());
        }

        return needUpdate;
    }

    private void CleanChunks()
    {
        foreach (Vector2Int p in chunks.Keys)
        {
            MarkChunkDirty(p, Math.Abs(chunkOffset.x - p.x) + Math.Abs(chunkOffset.y - p.y));
        }
    }

    private void UpdateChunkOffset(int xChange, int yChange)
    {
        if (xChange != 0)
        {
            for (int yChunk = -LoadDistance.y / 2; yChunk < LoadDistance.y / 2; yChunk++)
            {
                Vector2Int p = new Vector2Int(chunkOffset.x - xChange + ((int)-Mathf.Sign(xChange) * RenderDistance.x) / 2, yChunk + chunkOffset.y);
                if (chunks.ContainsKey(p))
                    MarkChunkDirty(p, Math.Abs(chunkOffset.x - p.x) + Math.Abs(chunkOffset.y - p.y));
            }
        }

        if (yChange != 0)
        {
            for (int xChunk = -LoadDistance.x / 2; xChunk < LoadDistance.x / 2; xChunk++)
            {
                Vector2Int p = new Vector2Int(xChunk + chunkOffset.x, chunkOffset.y - yChange + (-(int)Mathf.Sign(yChange) * RenderDistance.y) / 2);

                if (chunks.ContainsKey(p))
                    MarkChunkDirty(p, Math.Abs(chunkOffset.x - p.x) + Math.Abs(chunkOffset.y - p.y));
            }
        }
    }

    public Chunk GetContainingChunk(int x, int y)
    {
        if (x < 0)
            x -= ChunkSize - 1;

        if (y < 0)
            y -= ChunkSize - 1;

        return LoadChunk(x / ChunkSize, y / ChunkSize);
    }

    public bool isAir(int x, int y)
    {
        return GetTile(x, y) == air;
    }

    public Vector2Int GetTilePositionInWorld(Vector2Int chunkPos, int x, int y)
    {
        return new Vector2Int(chunkPos.x * ChunkSize + x, chunkPos.y * ChunkSize + y);
    }

    public Vector2Int GetTilePositionInChunk(int x, int y)
    {
        x %= ChunkSize;
        y %= ChunkSize;

        if (x < 0)
        {
            x = ChunkSize + x;
        }
        if (y < 0)
        {
            y = ChunkSize + y;
        }

        return new Vector2Int(x, y);
    }

    public Tile GetTile(int x, int y)
    {
        Vector2Int pos = GetTilePositionInChunk(x, y);

        return GetContainingChunk(x, y).GetTile(pos.x, pos.y);
    }

    public void SetTileById(int x, int y, int tileId)
    {
        SetTile(x, y, GetTileFromId(tileId));
    }

    public void SetTile(int x, int y, Tile tile)
    {
        Vector2Int pos = GetTilePositionInChunk(x, y);

        Chunk chunk = GetContainingChunk(x, y);
        Tile lastTile = chunk.GetTile(pos.x, pos.y);
        if (lastTile == tile) { return; }

        chunk.SetTile(pos.x, pos.y, tile);

        Vector2Int p;

        if (pos.x == ChunkSize - 1)
        {
            p = new Vector2Int(chunk.position.x + 1, chunk.position.y);
            if (chunks.ContainsKey(p))
                chunks[p].UpdateTile(0, pos.y, air, true);

        }
        else if (pos.x == 0)
        {
            p = new Vector2Int(chunk.position.x - 1, chunk.position.y);
            if (chunks.ContainsKey(p))
                chunks[p].UpdateTile((ChunkSize - 1), pos.y, air, true);

        }

        if (pos.y == ChunkSize - 1)
        {
            p = new Vector2Int(chunk.position.x, chunk.position.y + 1);
            if (chunks.ContainsKey(p))
                chunks[p].UpdateTile(pos.x, 0, air, true);

        }
        else if (pos.y == 0)
        {
            p = new Vector2Int(chunk.position.x, chunk.position.y - 1);
            if (chunks.ContainsKey(p))
                chunks[p].UpdateTile(pos.x, (ChunkSize - 1), air, true);

        }

        chunk.UpdateTile(pos.x, pos.y, lastTile, false);
    }

    public Tile GetTileFromId(int tile)
    {
        return tiles[tile];
    }

    public Tile GetTileFromName(string name)
    {
        return tileNameMap[name];
    }

    public Tile LoadTile(string name)
    {
        Tile existing;
        if (tileNameMap.TryGetValue(name, out existing))
        {
            return existing;
        }

        Tile t = new Tile();
        t.name = name;
        t.id = tiles.Count;
        t.texture = new TileTexture(Resources.Load("Tiles/" + name) as Texture2D);

        tiles.Add(t);
        tileNameMap.Add(name, t);

        Debug.Log("Loaded Tile: " + (tiles.Count - 1) + " " + name);

        return t;
    }

    // LIGHTING

    public float GetTileLight(int x, int y)
    {
        Vector2Int localPos = GetTilePositionInChunk(x, y);
        return GetContainingChunk(x, y).GetTileLight(localPos.x, localPos.y);
    }

    public void SetTileLight(int x, int y, float light)
    {
        Vector2Int localPos = GetTilePositionInChunk(x, y);
        SetTileLight(GetContainingChunk(x, y), localPos.x, localPos.y, light);
    }

    public void SetTileSunlight(int x, int y, float light)
    {
        Vector2Int localPos = GetTilePositionInChunk(x, y);
        SetTileLight(GetContainingChunk(x, y), localPos.x, localPos.y, light);
    }

    public float GetTileSunlight(int x, int y)
    {
        Vector2Int localPos = GetTilePositionInChunk(x, y);
        return GetContainingChunk(x, y).GetTileSunlight(localPos.x, localPos.y);
    }

    private void ApplyLighting()
    {
        if (lightProcessedChunks.Count > 0)
        {
            foreach (Chunk c in lightProcessedChunks)
            {
                if (c.instantiated)
                {
                    c.ApplyLighting();
                }
            }
            lightProcessedChunks.Clear();
        }
    }

    public void SetTileLight(Chunk chunk, int x, int y, float light)
    {
        if (light == 0)
        {
            RemoveTileLight(chunk, x, y);
            return;
        }

        int lightValue = Chunk.TileLightToValue(light);

        if (chunk.GetTileLightValue(x, y) == lightValue)
        {
            return;
        }

        chunk.SetTileLightValue(x, y, lightValue);

        Vector2Int tilePos = GetTilePositionInChunk(x, y);
        lightBfsQueue.Enqueue(new LightNode(tilePos.y * ChunkSize + tilePos.x, chunk, 0));

        StartCoroutine(ProcessLightQueue());

        //Debug.Log("Added Light: " + x + ", " + y + " : " + lightValue + " / " + light);
    }

    public IEnumerator ProcessLightQueue()
    {
        int processedLights = 0;
        while (lightBfsQueue.Count > 0)
        {
            LightNode node = lightBfsQueue.Dequeue();

            if (node.chunk.instantiated)
            {
                // Chunk-local tile position
                int x = node.index % ChunkSize;
                int y = node.index / ChunkSize;

                int lightLevel = node.chunk.GetTileLightValue(x, y);

                // Update four adjacent tiles
                AddAdjacentTileLight(node.chunk, x, y, -1, 0, lightLevel, node.priority);
                AddAdjacentTileLight(node.chunk, x, y, 1, 0, lightLevel, node.priority);
                AddAdjacentTileLight(node.chunk, x, y, 0, -1, lightLevel, node.priority);
                AddAdjacentTileLight(node.chunk, x, y, 0, 1, lightLevel, node.priority);

                if (node.priority > 0 && processedLights >= 32)
                {
                    processedLights = 0;
                    yield return null;
                }

                processedLights++;
            }
        }

        ApplyLighting();
    }

    private void AddAdjacentTileLight(Chunk chunk, int x, int y, int offsetX, int offsetY, int lightLevel, int priority)
    {
        int tileX = x + offsetX;
        int tileY = y + offsetY;

        int chunkX = chunk.position.x;
        int chunkY = chunk.position.y;

        // If adjacent tile is in neighbor chunk -> load that chunk and get local tile position
        if (tileX < 0 || tileX == ChunkSize || tileY < 0 || tileY == ChunkSize)
        {
            chunkX = tileX < 0 ? chunkX - 1 : (tileX == ChunkSize ? chunkX + 1 : chunkX);
            chunkY = tileY < 0 ? chunkY - 1 : (tileY == ChunkSize ? chunkY + 1 : chunkY);

            chunk = LoadChunk(chunkX, chunkY);

            tileX = tileX < 0 ? ChunkSize - 1 : (tileX == ChunkSize ? 0 : tileX);
            tileY = tileY < 0 ? ChunkSize - 1 : (tileY == ChunkSize ? 0 : tileY);
        }

        // If this tile still has light, enqueue it to propagate its light further
        if (chunk.GetTileLightValue(tileX, tileY) + 2 <= lightLevel)
        {
            chunk.SetTileLightValue(tileX, tileY, lightLevel - 1);

            if (chunk.displayed)
            {
                lightBfsQueue.Enqueue(new LightNode(tileY * ChunkSize + tileX, chunk, priority));
                lightProcessedChunks.Add(chunk);
            }
        }

    }

    public void RemoveTileLight(Chunk chunk, int x, int y)
    {
        int val = chunk.GetTileLightValue(x, y);
        int index = y * ChunkSize + x;

        lightRemovalBfsQueue.Enqueue(new LightRemovalNode(index, chunk, val));
        chunk.SetTileLightValue(x, y, 0);

        while (lightRemovalBfsQueue.Count > 0)
        {
            LightRemovalNode node = lightRemovalBfsQueue.Dequeue();

            chunk = node.chunk;

            // Chunk-local tile position
            x = node.index % ChunkSize;
            y = node.index / ChunkSize;

            RemoveAdjacentTileLight(chunk, x, y, -1, 0, node.value);
            RemoveAdjacentTileLight(chunk, x, y, 1, 0, node.value);
            RemoveAdjacentTileLight(chunk, x, y, 0, -1, node.value);
            RemoveAdjacentTileLight(chunk, x, y, 0, 1, node.value);
        }

        ApplyLighting();
        StartCoroutine(ProcessLightQueue());
    }

    private void RemoveAdjacentTileLight(Chunk chunk, int x, int y, int offsetX, int offsetY, int lightLevel)
    {
        int tileX = x + offsetX;
        int tileY = y + offsetY;

        int chunkX = chunk.position.x;
        int chunkY = chunk.position.y;

        // If adjacent tile is in neighbor chunk -> load that chunk and get local tile position
        if (tileX < 0 || tileX == ChunkSize || tileY < 0 || tileY == ChunkSize)
        {
            chunkX = tileX < 0 ? chunkX - 1 : (tileX == ChunkSize ? chunkX + 1 : chunk.position.x);
            chunkY = tileY < 0 ? chunkY - 1 : (tileY == ChunkSize ? chunkY + 1 : chunk.position.y);

            chunk = LoadChunk(chunkX, chunkY);

            tileX = tileX < 0 ? ChunkSize - 1 : (tileX == ChunkSize ? 0 : tileX);
            tileY = tileY < 0 ? ChunkSize - 1 : (tileY == ChunkSize ? 0 : tileY);
        }

        int neighborLevel = chunk.GetTileLightValue(tileX, tileY);

        if (neighborLevel != 0 && neighborLevel < lightLevel)
        {
            chunk.SetTileLightValue(tileX, tileY, 0);

            lightRemovalBfsQueue.Enqueue(new LightRemovalNode(tileY * ChunkSize + tileX, chunk, neighborLevel));
            lightProcessedChunks.Add(chunk);
        }
        else if (neighborLevel >= lightLevel)
        {
            lightBfsQueue.Enqueue(new LightNode(tileY * ChunkSize + tileX, chunk, 0));
            lightProcessedChunks.Add(chunk);
        }

    }

    // END LIGHTING

    public Chunk TryGetChunk(Vector2Int pos)
    {
        Chunk c;
        if (chunks.TryGetValue(pos, out c))
            return c;

        return null;
    }

    private Chunk LoadChunk(Vector2Int pos, bool instantiate = true)
    {
        Chunk c;
        if (chunks.TryGetValue(pos, out c))
            return c;

        if (cachedChunks.Count > 0)
        {
            c = cachedChunks.Dequeue();
            c.position = pos;
        }
        else
        {
            c = new Chunk(pos.x, pos.y);
        }

        if (instantiate)
        {
            c.Instantiate();
        }

        chunks.Add(pos, c);
        GenerateChunk(c);

        if (OnChunkLoaded != null)
        {
            OnChunkLoaded(c);
        }

        return c;
    }

    private void ToggleColliders(Vector2Int pos, bool on)
    {
        if (chunks.ContainsKey(pos))
            chunks[pos].ToggleColliders(on);
    }

    private Chunk LoadChunk(int x, int y)
    {
        return LoadChunk(new Vector2Int(x, y));
    }

    public void GenerateChunk(Chunk c)
    {
        Tile dirt = LoadTile("dirt");
        Tile grass = LoadTile("grass");

        int cHeight = 0;

        for (int x = 0; x < ChunkSize; x++)
        {
            int height = (int)Mathf.Round(32 * Mathf.PerlinNoise(640000 + (c.position.x * ChunkSize + x) / 32f, 0.0f));
            for (int y = 0; y < ChunkSize; y++)
            {
                int tileY = c.position.y * ChunkSize + y;

                if (tileY > cHeight + height)
                {
                    c.SetTile(x, y, air);

                }
                else
                {
                    int depth = cHeight + height - tileY;

                    if (depth <= 0)
                    {
                        c.SetTile(x, y, grass);
                        c.SetTileLight(x, y, 1f);
                    }
                    else
                    {
                        float cave = Mathf.PerlinNoise(640000 + (c.position.x * ChunkSize + x) / 16f, 640000 + (c.position.y * ChunkSize + y) / 16f);
                        bool isCave = cave < Mathf.Min(0.45f, 0.25f + Mathf.Min(0.1f, depth * 0.0001f));
                        isCave = depth > 16 ? isCave : false;

                        c.SetTile(x, y, isCave ? air : dirt);

                        if (isCave)
                        {
                            c.SetTileLight(x, y, 0.4f);
                        }
                    }

                }
            }
        }
    }

    private void CleanChunk(Vector2Int pos)
    {
        int xDist = Mathf.Abs(pos.x - chunkOffset.x);
        int yDist = Mathf.Abs(pos.y - chunkOffset.y);

        Chunk chunk = chunks[pos];
        if (xDist <= RenderDistance.x / 2 && yDist <= RenderDistance.y / 2)
        {
            if (chunk.needsTexture())
            {
                chunk.UpdateTexture();
            }

            if (!chunk.displayed && chunk.textured)
            {
                chunk.Display(true);

                for (int x = 0; x < ChunkSize; x++)
                {
                    for (int y = 0; y < ChunkSize; y++)
                    {
                        if (chunk.GetTileLightValue(x, y) > 0)
                        {
                            lightBfsQueue.Enqueue(new LightNode(y * ChunkSize + x, chunk, xDist + yDist));
                        }
                    }
                }

                StartCoroutine(ProcessLightQueue());
            }

        }
        else if (xDist > LoadDistance.x / 2 || yDist > LoadDistance.y / 2)
        {
            chunks.Remove(pos);

            if (cachedChunks.Count < LoadDistance.x * LoadDistance.y)
            {
                chunk.Cache();
                cachedChunks.Enqueue(chunk);
            }
            else
            {
                chunk.Unload();
            }

            if (OnChunkUnloaded != null)
            {
                OnChunkUnloaded(chunk);
            }
        }
        else if (chunk.displayed)
        {
            chunk.Display(false);
        }
    }

    public void GenerateChunks()
    {
        for (int xChunk = -LoadDistance.x / 2; xChunk < LoadDistance.x / 2; xChunk++)
        {
            for (int yChunk = -LoadDistance.y / 2; yChunk < LoadDistance.y / 2; yChunk++)
            {
                Vector2Int pos = new Vector2Int(chunkOffset.x + xChunk, chunkOffset.y + yChunk);

                Chunk c = LoadChunk(pos);
                c.ToggleColliders(true);

                MarkChunkDirty(pos, Math.Abs(chunkOffset.x - pos.x) + Math.Abs(chunkOffset.y - pos.y));
            }
        }
    }

    public void MarkChunkDirty(Vector2Int c, int priority = 0)
    {
        if (!dirtyChunks.Contains(c))
            dirtyChunks.Enqueue(c, priority);
    }

    public void MarkChunkDirty(Vector2Int c, int x, int y, int priority = 0)
    {
        MarkChunkDirty(new Vector2Int(c.x + x, c.y + y), priority);
    }

    public void MarkAllChunksDirty()
    {
        foreach (Vector2Int c in chunks.Keys)
        {
            MarkChunkDirty(c, Math.Abs(chunkOffset.x - c.x) + Math.Abs(chunkOffset.y - c.y));
        }
    }

    public bool[] GetTileBorders(int x, int y)
    {
        Tile tile = GetTile(x, y);

        return new bool[4] {
            GetTile (x, y + 1) != tile,
            GetTile(x + 1, y) != tile,
            GetTile(x, y - 1) != tile,
            GetTile(x - 1, y) != tile
        };
    }

    public bool IsBorderTile(int x, int y)
    {
        if (GetTile(x, y + 1) == air)
            return true;
        if (GetTile(x + 1, y) == air)
            return true;
        if (GetTile(x - 1, y) == air)
            return true;
        if (GetTile(x, y - 1) == air)
            return true;

        return false;
    }
}

public struct LightNode : System.IComparable
{
    public int index;
    public Chunk chunk;
    public int priority;

    public LightNode(int index, Chunk chunk, int priority)
    {
        this.index = index;
        this.chunk = chunk;
        this.priority = priority;
    }

    public int CompareTo(object obj)
    {
        return priority > ((LightNode)obj).priority ? 1 : priority < ((LightNode)obj).priority ? -1 : 0;
    }
}

public struct LightRemovalNode
{
    public int index;
    public int value;
    public Chunk chunk;

    public LightRemovalNode(int index, Chunk chunk, int value)
    {
        this.index = index;
        this.chunk = chunk;
        this.value = value;
    }
}