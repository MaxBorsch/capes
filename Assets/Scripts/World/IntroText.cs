﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroText : MonoBehaviour
{

    public float speed = 0.2f;

    void Update()
    {
        transform.Translate(new Vector3(0, Mathf.Cos(Time.time) * 0.05f, 0));

        float a = Mathf.Sin(Time.time * speed);
        transform.localScale = new Vector3(a, a, 1);
        transform.localRotation = Quaternion.Euler(0, 0, Mathf.Sin(Time.time * speed) * 10);
        if (a <= 0 && Time.time > 1)
        {
            Destroy(gameObject);
        }
    }
}
