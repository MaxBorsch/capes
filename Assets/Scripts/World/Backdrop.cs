﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backdrop : MonoBehaviour
{
    public float speed = 0.9f;
    public Texture2D skyBackground;
    public Texture2D caveBackground;

    private Renderer spriteRenderer;

    private void Start()
    {
        spriteRenderer = GetComponent<Renderer>();
        spriteRenderer.material.SetTexture("_MainTex", skyBackground);
        spriteRenderer.material.SetTexture("_TargetTex", caveBackground);
    }

    void Update()
    {
        transform.localScale = new Vector3(Game.WorldScreenWidth, Game.WorldScreenHeight);
        spriteRenderer.material.SetTextureOffset("_MainTex", new Vector2(Camera.main.transform.position.x * speed, Camera.main.transform.position.y * speed));
        spriteRenderer.material.SetFloat("_Transition", Mathf.Clamp01(Camera.main.transform.position.y / -16));
    }
}
