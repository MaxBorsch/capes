﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Tile
{
    public string name = "Unnamed Tile";
    public float durability = 0;
    public float light = 0;
    public TileTexture texture;

    [HideInInspector]
    public int id;
}