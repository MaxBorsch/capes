﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item
{
    public string name = "Unnamed Item";
    public Texture2D itemIcon = null;
    public Rigidbody itemObject = null;
    public int maxStackSize = 64;
    public int maxUses = 1;
}