﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileList : ScriptableObject
{
    public List<Tile> tiles;
}