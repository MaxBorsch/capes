﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hotbar : MonoBehaviour
{

    public GameObject UISlot;
    public Color selectedSlotColor;
    public Color deselectedSlotColor;
    public int selectedSlot;

    private ItemSlot[] slots;

    void Awake()
    {
        slots = new ItemSlot[10];

        for (int i = 0; i < 10; i++)
        {
            GameObject newSlot = Instantiate(UISlot.gameObject, gameObject.transform);
            newSlot.transform.Find("Hotkey").GetComponent<Text>().text = i.ToString();
            newSlot.GetComponent<Image>().material = new Material(newSlot.GetComponent<Image>().material);
            newSlot.GetComponent<Image>().material.SetColor("_BorderColor", i == selectedSlot ? selectedSlotColor : deselectedSlotColor);

            newSlot.name = "ItemSlot" + i;
            slots[i] = newSlot.GetComponent<ItemSlot>();
        }
    }

    public void SelectSlot(int slot)
    {
        if (slot >= slots.Length)
        {
            slot = 0;
        }
        else if (slot < 0)
        {
            slot = slots.Length - 1;
        }

        slots[selectedSlot].gameObject.GetComponent<Image>().material.SetColor("_BorderColor", deselectedSlotColor);

        selectedSlot = slot;

        slots[selectedSlot].gameObject.GetComponent<Image>().material.SetColor("_BorderColor", selectedSlotColor);
    }
}
