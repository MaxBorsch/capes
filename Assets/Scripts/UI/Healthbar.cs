﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{

    public GameObject heart;
    public Sprite[] heartSprites;
    public int heartCount = 20;
    public float health = 1;

    private GameObject[] hearts;
    private float lastHealth = 1;

    void Awake()
    {
        hearts = new GameObject[heartCount];

        for (int i = 0; i < heartCount; i++)
        {
            GameObject newSlot = Instantiate(heart, gameObject.transform);

            newSlot.name = "Heart" + i;

            hearts[i] = newSlot;
        }

        lastHealth = health;
    }

    void Update()
    {
        health = Mathf.Clamp01(health);

        if (health != lastHealth)
        {
            for (int i = 0; i < heartCount; i++)
            {
                if ((float)i / heartCount < health)
                {
                    float pDiff = Mathf.Clamp01(1f / heartCount / (health - (float)i / heartCount));
                    hearts[i].GetComponent<Image>().sprite = heartSprites[(int)Mathf.Round(pDiff * 3)];
                }
                else
                {
                    hearts[i].GetComponent<Image>().sprite = heartSprites[heartSprites.Length - 1];
                }
            }
        }

        lastHealth = health;
    }
}
