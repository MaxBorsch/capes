﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSlot : MonoBehaviour
{

    public Item item;

    public void switchItem(ItemSlot other)
    {
        Item old = item;
        item = other.item;
        other.item = old;
    }
}
