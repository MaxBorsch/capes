﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateInventoryItemList
{
    [MenuItem("Assets/Create/Inventory Item List")]
    public static ItemList Create()
    {
        ItemList asset = ScriptableObject.CreateInstance<ItemList>();

        AssetDatabase.CreateAsset(asset, "Assets/Items.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}