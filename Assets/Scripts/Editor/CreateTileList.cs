﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class CreateTileList
{
    [MenuItem("Assets/Create/Tile List")]
    public static TileList Create()
    {
        TileList asset = ScriptableObject.CreateInstance<TileList>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/Tiles.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}