﻿Shader "Custom/LitSprite"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _LightStrength ("Light Strength", Float) = 1
        _Color ("Tint", Color) = (1,1,1,1)
        _LightPos ("LightPos", Vector) = (0,0,0,0)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [MaterialToggle] _Pixelated ("Pixelated", Float) = 1
 
 
    }
 
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
 
        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha
 
        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
 
            #include "UnityCG.cginc"
           
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                float2 uv       : TEXCOORD1;
            };
 
            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldSpacePosition : float4;
                float2 uv       : TEXCOORD1;
            };  
           
            fixed4 _Color;
 
            v2f vert(appdata_t IN)
            {
                v2f OUT;
 
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
 
                #ifdef PIXELSNAP_ON
                    OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif
 
                OUT.worldSpacePosition = mul(unity_ObjectToWorld, OUT.vertex);
                OUT.uv = IN.uv;
                 
                return OUT;
            }
 
            sampler2D _MainTex;
            sampler2D _AlphaTex;
            float _LightStrength;
            Vector _LightPos;
            float _Pixelated;
             
            fixed4 SampleSpriteTexture (v2f IN)
            {
                float2 uv = IN.texcoord;
                float4 worldPos= IN.worldSpacePosition;
 
                //x and y are light position.
                float aS = worldPos.x-_LightPos.x;
                float bS = worldPos.y-_LightPos.y;
 
                if(_Pixelated == 1) {
                    float pixelWidth = 1.0f / _LightPos.z;
                    float pixelHeight = 1.0f / _LightPos.w;
                }
 
 
                fixed4 color = tex2D (_MainTex, uv);
 
#if ETC1_EXTERNAL_ALPHA
                // get the color from an external texture (usecase: Alpha support for ETC1 on android)
                color.a = tex2D (_AlphaTex, uv).r;
#endif //ETC1_EXTERNAL_ALPHA
 
                float dist = sqrt((aS * aS) + (bS * bS));
 
                //light strength.
                float percentClose = 1-(dist/_LightStrength);
 
                if(color.r + color.g + color.b > 0) {
                    color.r = lerp(0, color.r, percentClose);
                    color.g = lerp(0, color.g, percentClose);
                    color.b = lerp(0, color.b, percentClose);
                }
 
 
                return color;
            }
 
            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture (IN) * IN.color;
                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}