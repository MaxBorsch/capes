﻿Shader "Custom/Tile" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Emission("Emission (Lightmapper)", Float) = 1.0

	}

		SubShader{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 300

			CGPROGRAM
			#pragma target 3.0
			#pragma surface surf Lambert alpha nodynlightmap
			sampler2D _MainTex;
			uniform float _Lights[256];
			fixed4 _Color;
			fixed _Emission;

			struct Input {
				float2 uv_MainTex;
				float3 worldPos;
				//float3 vertexColor; // Vertex color stored here by vert() method
			};

			//void vert(inout appdata_full v, out Input o)
			//{
				//UNITY_INITIALIZE_OUTPUT(Input, o);
				//o.localPos = v.vertex.xyz;
				//o.vertexColor = v.color; // Save the Vertex Color in the Input for the surf() method
			//}

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
				fixed4 c = tex * _Color;
				o.Albedo = c.rgb;// *IN.vertexColor;

				float3 localPos = IN.worldPos -  mul(unity_ObjectToWorld, float4(0,0,0,1)).xyz;
				o.Emission = c.rgb * _Lights[floor(localPos.y) * 16 + floor(localPos.x)];

				#if defined (UNITY_PASS_META)
					o.Emission *= _Emission.rrr;
				#endif
				o.Alpha = c.a;
				//o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv2_BumpMap));
			}

			ENDCG
		}

		FallBack "Legacy Shaders/Self-Illumin/Diffuse"
		CustomEditor "LegacyIlluminShaderGUI"

}
