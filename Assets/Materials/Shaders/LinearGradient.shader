﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/LinearGradient"
 {
     Properties {
         _Color ("Bottom Color", Color) = (1,1,1,1)
         _Color2 ("Top Color", Color) = (1,1,1,1)
         _Scale ("Scale", Float) = 1
         _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
     }
      
     SubShader {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
         LOD 100
        
         ZWrite Off
         Blend SrcAlpha OneMinusSrcAlpha
        
         Pass {
             CGPROGRAM
                 #pragma vertex vert
                 #pragma fragment frag
                
                 #include "UnityCG.cginc"
                
                 struct appdata_t {
                     float4 vertex : POSITION;
                     float2 texcoord : TEXCOORD0;
                 };
      
                 struct v2f {
                     float4 vertex : SV_POSITION;
                     half2 texcoord : TEXCOORD0;
                     fixed4 col : COLOR;
                 };
      
                 sampler2D _MainTex;
                 float4 _MainTex_ST;
                 fixed4 _Color;
                 fixed4 _Color2;
                 fixed  _Scale;
 
                 v2f vert (appdata_t v)
                 {
                     v2f o;
                     o.col = lerp(_Color,_Color2, v.texcoord.y * _Scale);
                     o.vertex = UnityObjectToClipPos(v.vertex);
                     o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
 
                     return o;
                 }
                
                 fixed4 frag (v2f i) : SV_Target
                 {
                     fixed4 col = tex2D(_MainTex, i.texcoord) * i.col;
                     return col;
                 }
             ENDCG
         }
     }
  
 }