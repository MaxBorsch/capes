﻿Shader "Unlit/TextureTransition"
 {
     Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "" {}
		_TargetTex("Target (RGB)", 2D) = "" {}
		_Transition("Transition", Float) = 0.0

	}

		SubShader{
			Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 300

			CGPROGRAM
			#pragma target 3.0
			#pragma surface surf Lambert alpha nodynlightmap
			sampler2D _MainTex;
			sampler2D _TargetTex;
			fixed4 _Color;
			fixed _Transition;

			struct Input {
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o) {
				fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
				fixed4 tex2 = tex2D(_TargetTex, IN.uv_MainTex);
				fixed4 c = (tex * (1 - _Transition) + tex2 * _Transition) * _Color;
				o.Albedo = c.rgb;
				o.Emission = c.rgb;
				o.Alpha = c.a;
			}

			ENDCG
		}
 }